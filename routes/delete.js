const express = require("express");
const router = express.Router();
const { Customer } = require("../models/customer");

router.get("/", async (req, res) => {
    const customers = await Customer.find({
        state: { $eq: 1 },
    }).sort("name");
    res.render("recover", { customers: customers });
});

router.get("/:id", async (req, res) => {
    // let customer = await Customer.findByIdAndRemove(req.params.id);
    let customer = await Customer.findByIdAndUpdate(req.params.id, {
        state: 0,
    });
    if (!customer)
        return res
            .status(404)
            .send("The Customer with the given ID was not found.");
    res.redirect("/api/recover");
});

module.exports = router;
